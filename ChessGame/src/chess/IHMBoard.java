package chess;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

import pieces.Bishop;
import pieces.King;
import pieces.Knight;
import pieces.Pawn;
import pieces.Queen;
import pieces.Rook;

//IHMBoard is the main class for the game with graphics
public class IHMBoard extends JComponent {

	static final char SIDE_LETTERS[] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H' };
	static final int SIDE_NUMS[] = { 1, 2, 3, 4, 5, 6, 7, 8 };// symbols on the side of board
	public int x, y;
	public static int tempArray[][];
	public static String moveIHM;
	private static final long serialVersionUID = 4843477779374696065L;
	private static final int CASE_DIM = 50;// case
	Image imgwBi, imgbBi, imgwKi, imgbKi, imgwQu, imgbQu, imgwRo, imgbRo, imgwKn, imgbKn, imgwPa, imgbPa;
	Graphics2D g2;

	public IHMBoard() {

	}

	protected void paintComponent(Graphics g) {
		g2 = (Graphics2D) g;

		boolean isWhite = true;

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {

				// String namePiece = getTypePiece(i, j);wRo

				// g.drawImage(addImg(namePiece), AncrageIniX, AncrageIniY, this);

				if (isWhite) {
					g2.setPaint(Color.GRAY);
				} else {
					g2.setPaint(Color.darkGray);
				}
				g2.fill(new Rectangle2D.Double((j + 1) * CASE_DIM, (i + 1) * CASE_DIM, CASE_DIM, CASE_DIM));

				isWhite = !isWhite;
			}

			// TO DO ajuster aspect graphique avec la vrai map
			isWhite = !isWhite;
		}

		//
		g2.setPaint(Color.black);
		g2.setStroke(new BasicStroke(2));
		g2.draw(new Rectangle2D.Double(CASE_DIM, CASE_DIM, 8 * CASE_DIM, 8 * CASE_DIM));

//		this.addMouseListener(new MouseAdapter() {// provides empty implementation of all
//			// MouseListener`s methods, allowing us to
//			// override only those which interests us
//			public void mousePressed(MouseEvent e) {
//				if (e.getX() > 50 && e.getX() < 450 && e.getY() > 50 && e.getY() < 450) {
//					int k = 7, l = 0;
//					for (int i = 50; i <= 450; i = i + 50) {
//
//						for (int j = 50; j <= 450; j = j + 50) {
//							if (e.getX() > i && e.getX() < i + 50 && e.getY() > j && e.getY() < j + 50) {
//								moveIHM = SIDE_LETTERS[l] + "" + SIDE_NUMS[k];
//
//							}
//							k--;
//
//						}
//						k = 7;
//						l++;
//
//					}
//
//				}
//			}
//		});

		checkBoard();
		g2.dispose();

	}

	public void checkBoard() {

		for (int i = 0; i < 8; i++) { // looping through the board and printings symbols
			for (int j = 0; j < 8; j++) {

				if (Board.board[i][j].getSymbol().equals("wBi")) {
					g2.drawImage(addImg("wBi"), CASE_DIM * (j + 1), CASE_DIM * (i + 1), CASE_DIM - 1, CASE_DIM - 1,
							this);
				}
				if (Board.board[i][j].getSymbol().equals("bBi")) {
					g2.drawImage(addImg("bBi"), CASE_DIM * (j + 1), CASE_DIM * (i + 1), CASE_DIM - 1, CASE_DIM - 1,
							this);
				}
				if (Board.board[i][j].getSymbol().equals("wKi")) {
					g2.drawImage(addImg("wKi"), CASE_DIM * (j + 1), CASE_DIM * (i + 1), CASE_DIM - 1, CASE_DIM - 1,
							this);
				}
				if (Board.board[i][j].getSymbol().equals("bKi")) {
					g2.drawImage(addImg("bKi"), CASE_DIM * (j + 1), CASE_DIM * (i + 1), CASE_DIM - 1, CASE_DIM - 1,
							this);
				}
				if (Board.board[i][j].getSymbol().equals("wKn")) {
					g2.drawImage(addImg("wKn"), CASE_DIM * (j + 1), CASE_DIM * (i + 1), CASE_DIM - 1, CASE_DIM - 1,
							this);
				}
				if (Board.board[i][j].getSymbol().equals("bKn")) {
					g2.drawImage(addImg("bKn"), CASE_DIM * (j + 1), CASE_DIM * (i + 1), CASE_DIM - 1, CASE_DIM - 1,
							this);
				}
				if (Board.board[i][j].getSymbol().equals("wPa")) {
					g2.drawImage(addImg("wPa"), CASE_DIM * (j + 1), CASE_DIM * (i + 1), CASE_DIM - 1, CASE_DIM - 1,
							this);
				}
				if (Board.board[i][j].getSymbol().equals("bPa")) {
					g2.drawImage(addImg("bPa"), CASE_DIM * (j + 1), CASE_DIM * (i + 1), CASE_DIM - 1, CASE_DIM - 1,
							this);
				}
				if (Board.board[i][j].getSymbol().equals("wQu")) {
					g2.drawImage(addImg("wQu"), CASE_DIM * (j + 1), CASE_DIM * (i + 1), CASE_DIM - 1, CASE_DIM - 1,
							this);
				}
				if (Board.board[i][j].getSymbol().equals("bQu")) {
					g2.drawImage(addImg("bQu"), CASE_DIM * (j + 1), CASE_DIM * (i + 1), CASE_DIM - 1, CASE_DIM - 1,
							this);
				}
				if (Board.board[i][j].getSymbol().equals("wRo")) {
					g2.drawImage(addImg("wRo"), CASE_DIM * (j + 1), CASE_DIM * (i + 1), CASE_DIM - 1, CASE_DIM - 1,
							this);
				}
				if (Board.board[i][j].getSymbol().equals("bRo")) {
					g2.drawImage(addImg("bRo"), CASE_DIM * (j + 1), CASE_DIM * (i + 1), CASE_DIM - 1, CASE_DIM - 1,
							this);
				}

			}
		}
	}

	public Image addImg(String name) {
		if (name.equals("wBi")) {
			try {
				return imgwBi = ImageIO.read(new File("src/image/fou_blanc.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (name.equals("bBi")) {
			try {
				return imgbBi = ImageIO.read(new File("src/image/fou_noir.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (name.equals("wKn")) {
			try {
				return imgwKn = ImageIO.read(new File("src/image/cheval_blanc.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (name.equals("bKn")) {
			try {
				return imgbKn = ImageIO.read(new File("src/image/cheval_noir.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (name.equals("wKi")) {
			try {
				return imgwKi = ImageIO.read(new File("src/image/roi_blanc.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (name.equals("bKi")) {
			try {
				return imgbKi = ImageIO.read(new File("src/image/roi_noir.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (name.equals("wPa")) {
			try {
				return imgwPa = ImageIO.read(new File("src/image/pion_blanc.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (name.equals("bPa")) {
			try {
				return imgbPa = ImageIO.read(new File("src/image/pion_noir.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (name.equals("wQu")) {
			try {
				return imgwQu = ImageIO.read(new File("src/image/reine_blanc.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (name.equals("bQu")) {
			try {
				return imgbQu = ImageIO.read(new File("src/image/reine_noir.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (name.equals("wRo")) {
			try {
				return imgwRo = ImageIO.read(new File("src/image/tour_blanc.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (name.equals("bRo")) {
			try {
				return imgbRo = ImageIO.read(new File("src/image/tour_noir.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return null;

	}

}
